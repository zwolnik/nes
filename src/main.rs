#![feature(const_fn)]
use derive_more::{Display, From};
use env_logger;

mod cpu;

use cpu::mem::Mem;
use cpu::CPU;

fn main() {
    env_logger::init();
    let mut m = Mem::load_from_file("roms/6502_functional_test.bin").unwrap();
    let cpu = CPU::new(0x400);
    cpu.run(&mut m)
}


type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Display, Debug, From)]
pub enum Error {
    #[display(fmt = "{}", "_0")]
    IoError(std::io::Error),
    #[display(fmt = "{}", "_0")]
    OtherError(String),
}

impl<'a> From<&'a str> for Error {
    fn from(s: &'a str) -> Self {
        Error::OtherError(s.into())
    }
}
