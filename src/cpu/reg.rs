use num_traits::{Unsigned, WrappingAdd, WrappingSub};

#[derive(Copy, Clone)]
pub struct Reg<T>(T)
where
    T: Unsigned;
pub type Reg8 = Reg<u8>;
pub type Reg16 = Reg<u16>;

impl std::ops::BitOr<u8> for Reg<u8>
{
    type Output = u8;

    fn bitor(self, rhs: u8) -> u8 {
        let lhs: u8 = self.0.into();
        lhs | rhs
    }
}

impl<T> std::ops::BitOr<u16> for Reg<T>
where
    T: Unsigned,
    u16: From<T>,
{
    type Output = u16;

    fn bitor(self, rhs: u16) -> u16 {
        let lhs: u16 = self.0.into();
        lhs | rhs
    }
}

impl<T, V> std::ops::SubAssign<V> for Reg<T>
where
    T: Unsigned + From<V> + WrappingSub,
    V: Unsigned,
{
    fn sub_assign(&mut self, other: V) {
        let other: T = other.into();
        self.0 = self.0.wrapping_sub(&other);
    }
}

impl<T, V> std::ops::AddAssign<V> for Reg<T>
where
    T: Unsigned + From<V> + WrappingAdd,
    V: Unsigned,
{
    fn add_assign(&mut self, other: V) {
        let other: T = other.into();
        self.0 = self.0.wrapping_add(&other);
    }
}

impl From<u8> for Reg8 {
    fn from(val: u8) -> Self {
        Self(val)
    }
}

impl Into<u8> for Reg8 {
    fn into(self) -> u8 {
        self.0
    }
}

impl From<u16> for Reg16 {
    fn from(val: u16) -> Self {
        Self(val)
    }
}

impl Into<u16> for Reg16 {
    fn into(self) -> u16 {
        self.0
    }
}

impl Reg16 {
    pub fn lower(&self) -> u8 {
        self.0 as u8
    }

    pub fn higher(&self) -> u8 {
        (self.0 >> 8) as u8
    }

    pub fn set_l(&mut self, val: impl Into<u8>) {
        let val: u16 = val.into() as u16;
        self.0 = (self.0 & 0xFF00u16) | val;
    }

    pub fn set_h(&mut self, val: impl Into<u8>) {
        let val: u16 = (val.into() as u16) << 8;
        self.0 = (self.0 & 0x00FFu16) | val;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn getting_bytes_of_reg16() {
        let reg: Reg16 = Reg(0xABCD);
        assert_eq!(reg.lower(), 0xCDu8);
        assert_eq!(reg.higher(), 0xABu8);
    }

    #[test]
    fn setting_bytes_of_reg16() {
        let mut reg: Reg16 = Reg(0xFFFF);
        reg.set_l(0x34u8);
        reg.set_h(0x12u8);

        assert_eq!(reg.lower(), 0x34u8);
        assert_eq!(reg.higher(), 0x12u8);
        assert_eq!(0x1234u16, reg.into());
    }
} 
