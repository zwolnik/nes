use std::ops::{Deref, DerefMut};
use std::fs::read;
use super::reg::Reg16;
use crate::Result;

pub struct Mem {
    buffer: [u8; 0x10000],
}

impl Mem {
    pub fn new() -> Self {
        Self { buffer: [0; 0x10000] }
    }

    pub fn from_vec(vec: Vec<u8>) -> Self {
        let mut mem = Mem::new();
        &mut mem[..vec.len()].copy_from_slice(&vec[..]);
        mem
    }

    pub fn load_from_file(filename: &str) -> Result<Self> {
        let buf = read(filename)?;
        Ok(Mem::from_vec(buf))
    }

    pub fn read_byte(&self, ptr: impl Into<u16>) -> u8 {
        let ptr = ptr.into();
        self.buffer[ptr as usize]
    }

    pub fn write_byte(&mut self, ptr: impl Into<u16>, byte: u8) {
        let ptr = ptr.into();
        self.buffer[ptr as usize] = byte;
    }

    pub fn read_dbyte(&self, ptr: impl Into<u16>) -> u16 {
        let first_addr = ptr.into();
        let second_addr = first_addr.wrapping_add(1u16);
        ((self.read_byte(second_addr) as u16) << 8) | self.read_byte(first_addr) as u16
    }

    pub fn write_dbyte(&mut self, ptr: impl Into<u16>, dbyte: impl Into<Reg16>) {
        let first_addr = ptr.into();
        let second_addr = first_addr.wrapping_add(1u16);
        let dbyte = dbyte.into();
        let (first_byte, second_byte) = (dbyte.lower(), dbyte.higher());
        self.write_byte(first_addr, first_byte);
        self.write_byte(second_addr, second_byte);
    }
}

impl Deref for Mem {
    type Target = [u8];

    fn deref(&self) -> &[u8] {
        &self.buffer
    }
}

impl DerefMut for Mem {
    fn deref_mut(&mut self) -> &mut [u8] {
        &mut self.buffer[..]
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn reading_bytes_from_mem() {
        let mem = Mem::from_vec(vec![0x05u8, 0x08u8]);
        assert_eq!(0x05u8, mem.read_byte(0x0000u16));
        assert_eq!(0x08u8, mem.read_byte(0x0001u16));
        assert_eq!(0x0805u16, mem.read_dbyte(0x0000u16));
        // Check for wrapping around memory
        assert_eq!(0x0500u16, mem.read_dbyte(0xFFFFu16));
    }

    #[test]
    fn writing_bytes_to_mem() {
        let mut mem = Mem::new();
        mem.write_byte(0x0000u16, 0x03u8);
        mem.write_dbyte(0x0001u16, 0xAAFFu16);

        assert_eq!(0x03u8, mem.read_byte(0x0000u16));
        assert_eq!(0xFFu8, mem.read_byte(0x0001u16));
        assert_eq!(0xAAu8, mem.read_byte(0x0002u16));

        // Check for wrapping around memory
        mem.write_dbyte(0xFFFFu16, 0xAAFFu16);
        assert_eq!(0xFFu8, mem.read_byte(0xFFFFu16));
        assert_eq!(0xAAu8, mem.read_byte(0x0000u16));
    }
}
