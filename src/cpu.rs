mod reg;
pub mod mem;

use log::{trace, debug};

use reg::{Reg8, Reg16};
use mem::Mem;

enum Order {
    BRK = 0x00,
}

pub struct CPU {
    pub pc: Reg16,
    pub sp: Reg8,
    pub acc: Reg8,
    pub x: Reg8,
    pub y: Reg8,
    pub p: Reg8,
}

impl CPU {
    pub fn new(pc: impl Into<Reg16>) -> Self {
        Self {
            pc: pc.into(),
            sp: 0x00u8.into(),
            acc: 0x00u8.into(),
            x: 0x00u8.into(),
            y: 0x00u8.into(),
            p: 0x00u8.into(),
        }
    }

    pub fn run(mut self, mem: &mut Mem) -> ! {
        loop {
            let order = mem.read_byte(self.pc);
            self.execute_order(order, mem);
            self.pc += 0x001u16;
        }
    }

    fn execute_order(&mut self, order: u8, mem: &mut Mem) {
        use Order::*;
        match order {
            0x00 => { // BRK
                debug!("Executing BRK order");
                self.pc += 0x02u16;
                self.stack_push(self.pc.higher(), mem);
                self.stack_push(self.pc.lower(), mem);
                self.stack_push(self.p | 0b00010000u8, mem);
                self.pc.set_l(mem.read_byte(0xfffeu16));
                self.pc.set_h(mem.read_byte(0xffffu16));
            },
            _ => (),
        };
    }


    fn stack_push(&mut self, byte: u8, mem: &mut Mem) {
        mem.write_byte(self.sp | 0x100u16, byte);
        self.sp -= 0x01u8;
    }

    fn stack_pop(&mut self, mem: &Mem) -> u8 {
        self.sp += 0x01u8;
        mem.read_byte(self.sp | 0x100u16)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    const ROM_6502_FUNC_TEST: &'static [u8; 65536] =
        include_bytes!("../roms/6502_functional_test.bin");

    #[test]
    fn rom_6502_functional_test() {
        let _ = ROM_6502_FUNC_TEST;
    }
}
//#[test]
//fn test_sample_6502_functional_test() {
//    let mut context = DummyHost::new();
//    context.load_binary( 0x0000, ROM_6502_FUNCTIONAL_TEST );
//    context.set_pc( 0x400 );
//
//    loop {
//        let status = context.execute();
//        match status {
//            Ok( EmulationStatus::Normal ) => continue,
//            Ok( EmulationStatus::InfiniteLoop( pc ) ) => {
//                assert_eq!( pc, 0x3399 ); // Infinite loop at this address means success.
//                break;
//            },
//            Err(_) => assert!( false, format!( "{:?}", status ) )
//        }
//    }
//}
//
//#[test]
//fn test_sample_ttl6502() {
//    let mut context = DummyHost::new();
//    context.load_binary( 0xE000, ROM_TTL6502 );
//    context.reset();
//
//    loop {
//        /*
//         *             This is where TTL6502 starts to test the BCD mode
//         *             which has different behavior
//         *                         from the one which we emulate.
//         *                                 */
//        if context.pc() == 0xF5B6 {
//            break;
//        }
//
//        let status = context.execute();
//        assert_eq!( status, Ok( EmulationStatus::Normal ) );
//    }
//}
